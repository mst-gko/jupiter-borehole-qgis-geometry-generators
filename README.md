# Jupiter borehole QGIS geometry generators

Visualize boreholes with lithology through SQL and QGIS geometry generators of the GEUS Jupiter borehole database - https://jupiter.geus.dk/geusmap

## Get the database

Download the PCJupiterXL database from the [GEUS web site](https://www.geus.dk/produkter-ydelser-og-faciliteter/data-og-kort/national-boringsdatabase-jupiter/adgang-til-data/data-gennem-pcjupiter-og-pcjupiterxl-format).

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## SQL on PCJupiterXL

Create a table of this PostgreSQL SQL code


```
select l.boreholeid, b.boreholeno, b.use, b.purpose, ct.use_text,
       l.top, l.bottom, l.rocksymbol, rs.dgu_symbol,
       CASE
            WHEN RIGHT(rocksymbol, 1) = 's' THEN '#ff5400'
            WHEN RIGHT(rocksymbol, 1) = 'g' THEN '#ff5400'
            WHEN RIGHT(rocksymbol, 1) = 'q' THEN '#ff00ff'
            WHEN RIGHT(rocksymbol, 1) = 'a' THEN '#ff8000'
            WHEN RIGHT(rocksymbol, 1) = 'r' THEN '#ccb3ff'
            WHEN rocksymbol = 'ml' THEN '#dbb800'
            WHEN rocksymbol = 'dl'THEN '#ffba33'
            WHEN rocksymbol IN ('ol', 'pl', 'll') THEN '#000cff'
            WHEN rocksymbol = 'gl' THEN '#b3ffff'
            WHEN rocksymbol NOT IN ('dl', 'ml', 'ol', 'pl', 'll') AND RIGHT(rocksymbol, 1) = 'l' THEN '#9d711f'
            WHEN RIGHT(rocksymbol, 1) = 'j' THEN '#9d711f'
            WHEN RIGHT(rocksymbol, 1) = 'i' THEN '#faf59e'
            WHEN RIGHT(rocksymbol, 1) = 'k' THEN '#80ff99'
            WHEN RIGHT(rocksymbol, 1) = 't' THEN '#deff00'
            WHEN RIGHT(rocksymbol, 1) = 'p' THEN '#c16544'
            WHEN RIGHT(rocksymbol, 2) = 'ed' THEN '#52ffff'
            ELSE '#efefef'
        END AS html_color,
       b.geom
from jupiter.lithsamp l
inner join jupiter.borehole b on b.boreholeid = l.boreholeid
left join (select code, longtext use_text from jupiter.code where codetype = 855) ct on ct.code = b.use
left join (select code, longtext dgu_symbol from jupiter.code where codetype = 44) rs on rs.code = l.rocksymbol
order by l.boreholeid, l.top;
```

## QGIS geometry generator
Add a QGIS geometry generator style on table from SQL.

This is fine for views above scale 1:500
```
make_line(  
	make_point(x($geometry), y($geometry) + ("top"  ) ),
	make_point(x($geometry), y($geometry) + ("Bottom"  ) )
)
```

An extended version can be used for views below 1:500 - close up borehole viewing with layer change marker and lithology labels

## Download the geometry generator style as QGIS QML
-Single borehole view fittet for scale 1:1 - 1:500 [Download QML](qml/borehole_closeup_view.qml "Single borehole")

-Multiple borehole view QML fittet for scale > 1:500 [Download QML](qml/borehole_large_view "Larger view")

## Examples
![View1](img/view_jammerbugt.PNG "Jammerbugt")

![View2](img/view_single.PNG "Single borehole")

![View3](img/view_fur.PNG "Fur")

## License
GNUv3
