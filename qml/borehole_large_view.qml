<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="100000000" simplifyDrawingHints="0" readOnly="0" hasScaleBasedVisibilityFlag="1" maxScale="500" symbologyReferenceScale="-1" styleCategories="AllStyleCategories" labelsEnabled="1" simplifyAlgorithm="0" simplifyDrawingTol="1" simplifyLocal="1" simplifyMaxScale="1" version="3.22.3-Białowieża">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal endField="" fixedDuration="0" durationField="" endExpression="" limitMode="0" startExpression="" startField="" durationUnit="min" enabled="0" mode="0" accumulate="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 type="singleSymbol" forceraster="0" symbollevels="0" referencescale="-1" enableorderby="0">
    <symbols>
      <symbol type="marker" clip_to_extent="1" name="0" alpha="1" force_rhr="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" class="SimpleMarker" locked="0" enabled="1">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="0,255,255,246"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2.4"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="0,255,255,246"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="2.4"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer pass="0" class="GeometryGenerator" locked="0" enabled="1">
          <Option type="Map">
            <Option type="QString" name="SymbolType" value="Line"/>
            <Option type="QString" name="geometryModifier" value="make_line(  &#xd;&#xa;&#x9;make_point(x($geometry), y($geometry) + (&quot;top&quot;  ) ),&#xd;&#xa;&#x9;make_point(x($geometry), y($geometry) + (&quot;Bottom&quot;  ) )&#xd;&#xa;)&#xd;&#xa;"/>
            <Option type="QString" name="units" value="MM"/>
          </Option>
          <prop k="SymbolType" v="Line"/>
          <prop k="geometryModifier" v="make_line(  &#xd;&#xa;&#x9;make_point(x($geometry), y($geometry) + (&quot;top&quot;  ) ),&#xd;&#xa;&#x9;make_point(x($geometry), y($geometry) + (&quot;Bottom&quot;  ) )&#xd;&#xa;)&#xd;&#xa;"/>
          <prop k="units" v="MM"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <symbol type="line" clip_to_extent="1" name="@0@1" alpha="1" force_rhr="0">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer pass="0" class="SimpleLine" locked="0" enabled="1">
              <Option type="Map">
                <Option type="QString" name="align_dash_pattern" value="0"/>
                <Option type="QString" name="capstyle" value="square"/>
                <Option type="QString" name="customdash" value="5;2"/>
                <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="customdash_unit" value="MM"/>
                <Option type="QString" name="dash_pattern_offset" value="0"/>
                <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
                <Option type="QString" name="draw_inside_polygon" value="0"/>
                <Option type="QString" name="joinstyle" value="bevel"/>
                <Option type="QString" name="line_color" value="35,35,35,255"/>
                <Option type="QString" name="line_style" value="solid"/>
                <Option type="QString" name="line_width" value="2.06"/>
                <Option type="QString" name="line_width_unit" value="MM"/>
                <Option type="QString" name="offset" value="0"/>
                <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="offset_unit" value="MM"/>
                <Option type="QString" name="ring_filter" value="0"/>
                <Option type="QString" name="trim_distance_end" value="0"/>
                <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="trim_distance_end_unit" value="MM"/>
                <Option type="QString" name="trim_distance_start" value="0"/>
                <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="trim_distance_start_unit" value="MM"/>
                <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
                <Option type="QString" name="use_custom_dash" value="0"/>
                <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              </Option>
              <prop k="align_dash_pattern" v="0"/>
              <prop k="capstyle" v="square"/>
              <prop k="customdash" v="5;2"/>
              <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="customdash_unit" v="MM"/>
              <prop k="dash_pattern_offset" v="0"/>
              <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="dash_pattern_offset_unit" v="MM"/>
              <prop k="draw_inside_polygon" v="0"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="line_color" v="35,35,35,255"/>
              <prop k="line_style" v="solid"/>
              <prop k="line_width" v="2.06"/>
              <prop k="line_width_unit" v="MM"/>
              <prop k="offset" v="0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="ring_filter" v="0"/>
              <prop k="trim_distance_end" v="0"/>
              <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="trim_distance_end_unit" v="MM"/>
              <prop k="trim_distance_start" v="0"/>
              <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="trim_distance_start_unit" v="MM"/>
              <prop k="tweak_dash_pattern_on_corners" v="0"/>
              <prop k="use_custom_dash" v="0"/>
              <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" name="name" value=""/>
                  <Option type="Map" name="properties">
                    <Option type="Map" name="outlineColor">
                      <Option type="bool" name="active" value="true"/>
                      <Option type="QString" name="expression" value="&quot;html_color&quot;"/>
                      <Option type="int" name="type" value="3"/>
                    </Option>
                  </Option>
                  <Option type="QString" name="type" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style useSubstitutions="0" previewBkgrdColor="255,255,255,255" fontWordSpacing="0" isExpression="0" fontItalic="0" textColor="50,50,50,255" fontSize="10" multilineHeight="1" textOpacity="1" blendMode="0" fontFamily="Liberation Sans" fontSizeMapUnitScale="3x:0,0,0,0,0,0" allowHtml="0" capitalization="0" fontKerning="1" fontUnderline="0" textOrientation="horizontal" namedStyle="Regular" fontSizeUnit="Point" fontStrikeout="0" fieldName="boreholeno" fontLetterSpacing="0" fontWeight="50" legendString="Aa">
        <families/>
        <text-buffer bufferOpacity="1" bufferNoFill="1" bufferBlendMode="0" bufferColor="250,250,250,255" bufferDraw="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferSize="1" bufferSizeUnits="MM" bufferJoinStyle="128"/>
        <text-mask maskSizeUnits="MM" maskJoinStyle="128" maskOpacity="1" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskSize="0" maskedSymbolLayers="" maskEnabled="0" maskType="0"/>
        <background shapeRadiiY="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidthUnit="Point" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetY="0" shapeRotation="0" shapeFillColor="255,255,255,255" shapeOffsetX="0" shapeBorderColor="128,128,128,255" shapeSVGFile="" shapeOpacity="1" shapeRotationType="0" shapeJoinStyle="64" shapeSizeY="0" shapeType="0" shapeRadiiUnit="Point" shapeSizeType="0" shapeDraw="0" shapeBorderWidth="0" shapeBlendMode="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiX="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeOffsetUnit="Point" shapeSizeUnit="Point">
          <symbol type="marker" clip_to_extent="1" name="markerSymbol" alpha="1" force_rhr="0">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer pass="0" class="SimpleMarker" locked="0" enabled="1">
              <Option type="Map">
                <Option type="QString" name="angle" value="0"/>
                <Option type="QString" name="cap_style" value="square"/>
                <Option type="QString" name="color" value="133,182,111,255"/>
                <Option type="QString" name="horizontal_anchor_point" value="1"/>
                <Option type="QString" name="joinstyle" value="bevel"/>
                <Option type="QString" name="name" value="circle"/>
                <Option type="QString" name="offset" value="0,0"/>
                <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="offset_unit" value="MM"/>
                <Option type="QString" name="outline_color" value="35,35,35,255"/>
                <Option type="QString" name="outline_style" value="solid"/>
                <Option type="QString" name="outline_width" value="0"/>
                <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="outline_width_unit" value="MM"/>
                <Option type="QString" name="scale_method" value="diameter"/>
                <Option type="QString" name="size" value="2"/>
                <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="size_unit" value="MM"/>
                <Option type="QString" name="vertical_anchor_point" value="1"/>
              </Option>
              <prop k="angle" v="0"/>
              <prop k="cap_style" v="square"/>
              <prop k="color" v="133,182,111,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="circle"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" name="name" value=""/>
                  <Option name="properties"/>
                  <Option type="QString" name="type" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
          <symbol type="fill" clip_to_extent="1" name="fillSymbol" alpha="1" force_rhr="0">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer pass="0" class="SimpleFill" locked="0" enabled="1">
              <Option type="Map">
                <Option type="QString" name="border_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="color" value="255,255,255,255"/>
                <Option type="QString" name="joinstyle" value="bevel"/>
                <Option type="QString" name="offset" value="0,0"/>
                <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
                <Option type="QString" name="offset_unit" value="MM"/>
                <Option type="QString" name="outline_color" value="128,128,128,255"/>
                <Option type="QString" name="outline_style" value="no"/>
                <Option type="QString" name="outline_width" value="0"/>
                <Option type="QString" name="outline_width_unit" value="Point"/>
                <Option type="QString" name="style" value="solid"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="255,255,255,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="128,128,128,255"/>
              <prop k="outline_style" v="no"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="Point"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" name="name" value=""/>
                  <Option name="properties"/>
                  <Option type="QString" name="type" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOpacity="0.69999999999999996" shadowOffsetGlobal="1" shadowColor="0,0,0,255" shadowBlendMode="6" shadowRadiusAlphaOnly="0" shadowRadius="1.5" shadowRadiusUnit="MM" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowScale="100" shadowDraw="0" shadowUnder="0" shadowOffsetAngle="135" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetDist="1" shadowOffsetUnit="MM"/>
        <dd_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format formatNumbers="0" wrapChar="" useMaxLineLengthForAutoWrap="1" multilineAlign="3" decimals="3" plussign="0" rightDirectionSymbol=">" reverseDirectionSymbol="0" leftDirectionSymbol="&lt;" addDirectionSymbol="0" autoWrapLength="0" placeDirectionSymbol="0"/>
      <placement offsetType="1" distUnits="MM" repeatDistanceUnits="MM" rotationUnit="AngleDegrees" lineAnchorPercent="0.5" overrunDistanceUnit="MM" fitInPolygonOnly="0" geometryGeneratorEnabled="0" centroidInside="0" preserveRotation="1" polygonPlacementFlags="2" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" quadOffset="1" offsetUnits="MM" dist="0" yOffset="-1" placementFlags="10" lineAnchorClipping="0" repeatDistance="0" xOffset="0" maxCurvedCharAngleOut="-25" maxCurvedCharAngleIn="25" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" distMapUnitScale="3x:0,0,0,0,0,0" geometryGenerator="" placement="1" priority="5" geometryGeneratorType="PointGeometry" lineAnchorType="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" rotationAngle="0" centroidWhole="0" layerType="PointGeometry" overrunDistance="0"/>
      <rendering fontMaxPixelSize="10000" fontLimitPixelSize="0" obstacle="1" upsidedownLabels="0" mergeLines="0" minFeatureSize="0" fontMinPixelSize="3" scaleVisibility="0" limitNumLabels="0" obstacleFactor="1" zIndex="0" obstacleType="1" maxNumLabels="2000" scaleMin="0" scaleMax="0" labelPerPart="0" displayAll="0" drawLabels="1" unplacedVisibility="0"/>
      <dd_properties>
        <Option type="Map">
          <Option type="QString" name="name" value=""/>
          <Option name="properties"/>
          <Option type="QString" name="type" value="collection"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option type="QString" name="anchorPoint" value="pole_of_inaccessibility"/>
          <Option type="int" name="blendMode" value="0"/>
          <Option type="Map" name="ddProperties">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
          <Option type="bool" name="drawToAllParts" value="false"/>
          <Option type="QString" name="enabled" value="0"/>
          <Option type="QString" name="labelAnchorPoint" value="point_on_exterior"/>
          <Option type="QString" name="lineSymbol" value="&lt;symbol type=&quot;line&quot; clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; alpha=&quot;1&quot; force_rhr=&quot;0&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer pass=&quot;0&quot; class=&quot;SimpleLine&quot; locked=&quot;0&quot; enabled=&quot;1&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;align_dash_pattern&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;capstyle&quot; value=&quot;square&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;customdash&quot; value=&quot;5;2&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;customdash_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;customdash_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;dash_pattern_offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;dash_pattern_offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;dash_pattern_offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;draw_inside_polygon&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;joinstyle&quot; value=&quot;bevel&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;line_color&quot; value=&quot;60,60,60,255&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;line_style&quot; value=&quot;solid&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;line_width&quot; value=&quot;0.3&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;line_width_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;offset_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;ring_filter&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_end&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_end_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_end_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_start&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_start_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;trim_distance_start_unit&quot; value=&quot;MM&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;tweak_dash_pattern_on_corners&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;use_custom_dash&quot; value=&quot;0&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;width_map_unit_scale&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;/Option>&lt;prop k=&quot;align_dash_pattern&quot; v=&quot;0&quot;/>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;dash_pattern_offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;dash_pattern_offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;dash_pattern_offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;trim_distance_end&quot; v=&quot;0&quot;/>&lt;prop k=&quot;trim_distance_end_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;trim_distance_end_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;trim_distance_start&quot; v=&quot;0&quot;/>&lt;prop k=&quot;trim_distance_start_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;trim_distance_start_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;tweak_dash_pattern_on_corners&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; name=&quot;name&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; name=&quot;type&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>"/>
          <Option type="double" name="minLength" value="0"/>
          <Option type="QString" name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0"/>
          <Option type="QString" name="minLengthUnit" value="MM"/>
          <Option type="double" name="offsetFromAnchor" value="0"/>
          <Option type="QString" name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0"/>
          <Option type="QString" name="offsetFromAnchorUnit" value="MM"/>
          <Option type="double" name="offsetFromLabel" value="0"/>
          <Option type="QString" name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0"/>
          <Option type="QString" name="offsetFromLabelUnit" value="MM"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;boreholeno&quot;"/>
      </Option>
      <Option type="int" name="embeddedWidgets/count" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory width="15" lineSizeScale="3x:0,0,0,0,0,0" spacingUnit="MM" sizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" penColor="#000000" enabled="0" scaleDependency="Area" penAlpha="255" diagramOrientation="Up" minScaleDenominator="500" spacingUnitScale="3x:0,0,0,0,0,0" showAxis="1" backgroundAlpha="255" barWidth="5" scaleBasedVisibility="0" opacity="1" direction="0" maxScaleDenominator="1e+08" height="15" lineSizeType="MM" rotationOffset="270" backgroundColor="#ffffff" sizeType="MM" minimumSize="0" spacing="5" penWidth="0">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
      <attribute field="" label="" color="#000000"/>
      <axisSymbol>
        <symbol type="line" clip_to_extent="1" name="" alpha="1" force_rhr="0">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer pass="0" class="SimpleLine" locked="0" enabled="1">
            <Option type="Map">
              <Option type="QString" name="align_dash_pattern" value="0"/>
              <Option type="QString" name="capstyle" value="square"/>
              <Option type="QString" name="customdash" value="5;2"/>
              <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="customdash_unit" value="MM"/>
              <Option type="QString" name="dash_pattern_offset" value="0"/>
              <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
              <Option type="QString" name="draw_inside_polygon" value="0"/>
              <Option type="QString" name="joinstyle" value="bevel"/>
              <Option type="QString" name="line_color" value="35,35,35,255"/>
              <Option type="QString" name="line_style" value="solid"/>
              <Option type="QString" name="line_width" value="0.26"/>
              <Option type="QString" name="line_width_unit" value="MM"/>
              <Option type="QString" name="offset" value="0"/>
              <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offset_unit" value="MM"/>
              <Option type="QString" name="ring_filter" value="0"/>
              <Option type="QString" name="trim_distance_end" value="0"/>
              <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_end_unit" value="MM"/>
              <Option type="QString" name="trim_distance_start" value="0"/>
              <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_start_unit" value="MM"/>
              <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
              <Option type="QString" name="use_custom_dash" value="0"/>
              <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" priority="0" zIndex="0" obstacle="0" showAll="1" dist="0" placement="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="boreholeid" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boreholeno" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="use" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="purpose" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="use_text" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="top" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="bottom" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="rocksymbol" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dgu_symbol" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="html_color" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="boreholeid" index="0"/>
    <alias name="" field="boreholeno" index="1"/>
    <alias name="" field="use" index="2"/>
    <alias name="" field="purpose" index="3"/>
    <alias name="" field="use_text" index="4"/>
    <alias name="" field="top" index="5"/>
    <alias name="" field="bottom" index="6"/>
    <alias name="" field="rocksymbol" index="7"/>
    <alias name="" field="dgu_symbol" index="8"/>
    <alias name="" field="html_color" index="9"/>
  </aliases>
  <defaults>
    <default expression="" field="boreholeid" applyOnUpdate="0"/>
    <default expression="" field="boreholeno" applyOnUpdate="0"/>
    <default expression="" field="use" applyOnUpdate="0"/>
    <default expression="" field="purpose" applyOnUpdate="0"/>
    <default expression="" field="use_text" applyOnUpdate="0"/>
    <default expression="" field="top" applyOnUpdate="0"/>
    <default expression="" field="bottom" applyOnUpdate="0"/>
    <default expression="" field="rocksymbol" applyOnUpdate="0"/>
    <default expression="" field="dgu_symbol" applyOnUpdate="0"/>
    <default expression="" field="html_color" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="boreholeid"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="boreholeno"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="use"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="purpose"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="use_text"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="top"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="bottom"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="rocksymbol"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="dgu_symbol"/>
    <constraint constraints="0" unique_strength="0" notnull_strength="0" exp_strength="0" field="html_color"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="boreholeid" desc=""/>
    <constraint exp="" field="boreholeno" desc=""/>
    <constraint exp="" field="use" desc=""/>
    <constraint exp="" field="purpose" desc=""/>
    <constraint exp="" field="use_text" desc=""/>
    <constraint exp="" field="top" desc=""/>
    <constraint exp="" field="bottom" desc=""/>
    <constraint exp="" field="rocksymbol" desc=""/>
    <constraint exp="" field="dgu_symbol" desc=""/>
    <constraint exp="" field="html_color" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
    <actionsetting type="5" name="Åben borerapport" isEnabledOnlyWhenEditable="0" notificationMessage="" id="{9fb738e3-0b3d-4e75-a11e-f032eefe01f5}" action="'https://data.geus.dk/JupiterWWW/borerapport.jsp?borid='[%boreholeid%]" icon="" shortTitle="Åben borerapport" capture="0">
      <actionScope id="Field"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;top&quot;" sortOrder="0" actionWidgetStyle="dropDown">
    <columns>
      <column type="field" name="boreholeid" width="-1" hidden="0"/>
      <column type="field" name="boreholeno" width="-1" hidden="0"/>
      <column type="field" name="top" width="-1" hidden="0"/>
      <column type="field" name="bottom" width="-1" hidden="0"/>
      <column type="field" name="rocksymbol" width="-1" hidden="0"/>
      <column type="field" name="use" width="-1" hidden="0"/>
      <column type="field" name="purpose" width="-1" hidden="0"/>
      <column type="field" name="use_text" width="-1" hidden="0"/>
      <column type="field" name="dgu_symbol" width="-1" hidden="0"/>
      <column type="field" name="html_color" width="-1" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="analyses" editable="1"/>
    <field name="bagno" editable="1"/>
    <field name="boreholeid" editable="1"/>
    <field name="boreholeno" editable="1"/>
    <field name="bottom" editable="1"/>
    <field name="calcareous" editable="1"/>
    <field name="cementatio" editable="1"/>
    <field name="classifica" editable="1"/>
    <field name="color" editable="1"/>
    <field name="depestim" editable="1"/>
    <field name="dgu_symbol" editable="1"/>
    <field name="diagenesis" editable="1"/>
    <field name="drillbagno" editable="1"/>
    <field name="drillcolor" editable="1"/>
    <field name="drillcolsb" editable="1"/>
    <field name="drilldescr" editable="1"/>
    <field name="drillremrk" editable="1"/>
    <field name="drillrockt" editable="1"/>
    <field name="drillsampleref" editable="1"/>
    <field name="fossils" editable="1"/>
    <field name="grainshape" editable="1"/>
    <field name="guid" editable="1"/>
    <field name="guid_borehole" editable="1"/>
    <field name="hardness" editable="1"/>
    <field name="html_color" editable="1"/>
    <field name="insertdate" editable="1"/>
    <field name="insertuser" editable="1"/>
    <field name="longtext" editable="1"/>
    <field name="minerals" editable="1"/>
    <field name="minorcomps" editable="1"/>
    <field name="munsellcolor" editable="1"/>
    <field name="oldcolor" editable="1"/>
    <field name="otherdescr" editable="1"/>
    <field name="purpose" editable="1"/>
    <field name="remarks" editable="1"/>
    <field name="rocksymbol" editable="1"/>
    <field name="rocktype" editable="1"/>
    <field name="rounding" editable="1"/>
    <field name="row_num" editable="1"/>
    <field name="samplebottom" editable="1"/>
    <field name="sampledep" editable="1"/>
    <field name="sampleid" editable="1"/>
    <field name="samplekept" editable="1"/>
    <field name="sampleno" editable="1"/>
    <field name="sampletop" editable="1"/>
    <field name="sampletype" editable="1"/>
    <field name="sorting" editable="1"/>
    <field name="structure" editable="1"/>
    <field name="texture" editable="1"/>
    <field name="thickness" editable="1"/>
    <field name="top" editable="1"/>
    <field name="top_acc" editable="1"/>
    <field name="totaldescr" editable="1"/>
    <field name="trivialnam" editable="1"/>
    <field name="updatedate" editable="1"/>
    <field name="updateuser" editable="1"/>
    <field name="use" editable="1"/>
    <field name="use_text" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="analyses" labelOnTop="0"/>
    <field name="bagno" labelOnTop="0"/>
    <field name="boreholeid" labelOnTop="0"/>
    <field name="boreholeno" labelOnTop="0"/>
    <field name="bottom" labelOnTop="0"/>
    <field name="calcareous" labelOnTop="0"/>
    <field name="cementatio" labelOnTop="0"/>
    <field name="classifica" labelOnTop="0"/>
    <field name="color" labelOnTop="0"/>
    <field name="depestim" labelOnTop="0"/>
    <field name="dgu_symbol" labelOnTop="0"/>
    <field name="diagenesis" labelOnTop="0"/>
    <field name="drillbagno" labelOnTop="0"/>
    <field name="drillcolor" labelOnTop="0"/>
    <field name="drillcolsb" labelOnTop="0"/>
    <field name="drilldescr" labelOnTop="0"/>
    <field name="drillremrk" labelOnTop="0"/>
    <field name="drillrockt" labelOnTop="0"/>
    <field name="drillsampleref" labelOnTop="0"/>
    <field name="fossils" labelOnTop="0"/>
    <field name="grainshape" labelOnTop="0"/>
    <field name="guid" labelOnTop="0"/>
    <field name="guid_borehole" labelOnTop="0"/>
    <field name="hardness" labelOnTop="0"/>
    <field name="html_color" labelOnTop="0"/>
    <field name="insertdate" labelOnTop="0"/>
    <field name="insertuser" labelOnTop="0"/>
    <field name="longtext" labelOnTop="0"/>
    <field name="minerals" labelOnTop="0"/>
    <field name="minorcomps" labelOnTop="0"/>
    <field name="munsellcolor" labelOnTop="0"/>
    <field name="oldcolor" labelOnTop="0"/>
    <field name="otherdescr" labelOnTop="0"/>
    <field name="purpose" labelOnTop="0"/>
    <field name="remarks" labelOnTop="0"/>
    <field name="rocksymbol" labelOnTop="0"/>
    <field name="rocktype" labelOnTop="0"/>
    <field name="rounding" labelOnTop="0"/>
    <field name="row_num" labelOnTop="0"/>
    <field name="samplebottom" labelOnTop="0"/>
    <field name="sampledep" labelOnTop="0"/>
    <field name="sampleid" labelOnTop="0"/>
    <field name="samplekept" labelOnTop="0"/>
    <field name="sampleno" labelOnTop="0"/>
    <field name="sampletop" labelOnTop="0"/>
    <field name="sampletype" labelOnTop="0"/>
    <field name="sorting" labelOnTop="0"/>
    <field name="structure" labelOnTop="0"/>
    <field name="texture" labelOnTop="0"/>
    <field name="thickness" labelOnTop="0"/>
    <field name="top" labelOnTop="0"/>
    <field name="top_acc" labelOnTop="0"/>
    <field name="totaldescr" labelOnTop="0"/>
    <field name="trivialnam" labelOnTop="0"/>
    <field name="updatedate" labelOnTop="0"/>
    <field name="updateuser" labelOnTop="0"/>
    <field name="use" labelOnTop="0"/>
    <field name="use_text" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="analyses" reuseLastValue="0"/>
    <field name="bagno" reuseLastValue="0"/>
    <field name="boreholeid" reuseLastValue="0"/>
    <field name="boreholeno" reuseLastValue="0"/>
    <field name="bottom" reuseLastValue="0"/>
    <field name="calcareous" reuseLastValue="0"/>
    <field name="cementatio" reuseLastValue="0"/>
    <field name="classifica" reuseLastValue="0"/>
    <field name="color" reuseLastValue="0"/>
    <field name="depestim" reuseLastValue="0"/>
    <field name="dgu_symbol" reuseLastValue="0"/>
    <field name="diagenesis" reuseLastValue="0"/>
    <field name="drillbagno" reuseLastValue="0"/>
    <field name="drillcolor" reuseLastValue="0"/>
    <field name="drillcolsb" reuseLastValue="0"/>
    <field name="drilldescr" reuseLastValue="0"/>
    <field name="drillremrk" reuseLastValue="0"/>
    <field name="drillrockt" reuseLastValue="0"/>
    <field name="drillsampleref" reuseLastValue="0"/>
    <field name="fossils" reuseLastValue="0"/>
    <field name="grainshape" reuseLastValue="0"/>
    <field name="guid" reuseLastValue="0"/>
    <field name="guid_borehole" reuseLastValue="0"/>
    <field name="hardness" reuseLastValue="0"/>
    <field name="html_color" reuseLastValue="0"/>
    <field name="insertdate" reuseLastValue="0"/>
    <field name="insertuser" reuseLastValue="0"/>
    <field name="longtext" reuseLastValue="0"/>
    <field name="minerals" reuseLastValue="0"/>
    <field name="minorcomps" reuseLastValue="0"/>
    <field name="munsellcolor" reuseLastValue="0"/>
    <field name="oldcolor" reuseLastValue="0"/>
    <field name="otherdescr" reuseLastValue="0"/>
    <field name="purpose" reuseLastValue="0"/>
    <field name="remarks" reuseLastValue="0"/>
    <field name="rocksymbol" reuseLastValue="0"/>
    <field name="rocktype" reuseLastValue="0"/>
    <field name="rounding" reuseLastValue="0"/>
    <field name="row_num" reuseLastValue="0"/>
    <field name="samplebottom" reuseLastValue="0"/>
    <field name="sampledep" reuseLastValue="0"/>
    <field name="sampleid" reuseLastValue="0"/>
    <field name="samplekept" reuseLastValue="0"/>
    <field name="sampleno" reuseLastValue="0"/>
    <field name="sampletop" reuseLastValue="0"/>
    <field name="sampletype" reuseLastValue="0"/>
    <field name="sorting" reuseLastValue="0"/>
    <field name="structure" reuseLastValue="0"/>
    <field name="texture" reuseLastValue="0"/>
    <field name="thickness" reuseLastValue="0"/>
    <field name="top" reuseLastValue="0"/>
    <field name="top_acc" reuseLastValue="0"/>
    <field name="totaldescr" reuseLastValue="0"/>
    <field name="trivialnam" reuseLastValue="0"/>
    <field name="updatedate" reuseLastValue="0"/>
    <field name="updateuser" reuseLastValue="0"/>
    <field name="use" reuseLastValue="0"/>
    <field name="use_text" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"boreholeno"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
